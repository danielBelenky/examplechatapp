from flask import Flask, render_template, request, redirect
from time import asctime
import messages


app = Flask(__name__)


@app.route('/')
def compose_message():
    all_messages = messages.get_messages_from_store()
    return render_template(
        'messages.html',
        current_time=asctime(),
        messages=all_messages
    )


@app.route('/store_message/')
def store_message():
    sender = request.args.get('from')
    title = request.args.get('title')
    message = request.args.get('message')
    time_on_message = request.args.get('time')
    messages.store_new_message(sender, title, message, time_on_message)
    return redirect('/')