import pytest
import messages

def test_store_message_on_empty_start(monkeypatch, tmp_path):
    tmp_message_store = tmp_path / 'message_store'
    messages.store_new_message(
        sender='test sender 1',
        title='test title 1',
        message='test message 1',
        time_on_message='some time 1',
        store=tmp_message_store
    )
    messages.store_new_message(
        sender='test sender 2',
        title='test title 2',
        message='test message 2',
        time_on_message='some time 2',
        store=tmp_message_store
    )
    assert tmp_message_store.read_text() == (
        '- message: test message 1\n'
        '  sender: test sender 1\n'
        '  time: some time 1\n'
        '  title: test title 1\n'
        '- message: test message 2\n'
        '  sender: test sender 2\n'
        '  time: some time 2\n'
        '  title: test title 2\n'
    )