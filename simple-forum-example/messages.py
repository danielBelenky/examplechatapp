"""
This module is dealing with messages
"""
import yaml


DEFAULT_STORE = '/tmp/message_store'


def store_new_message(
    sender, title, message, time_on_message, store=None
):
    """Store a new message in the messages store.
    """
    if store is None:
        store = DEFAULT_STORE
    new_message = [{
        'title': title,
        'message': message,
        'sender': sender,
        'time': time_on_message,
    }]
    try:
        with open(store, 'r') as message_store:
            messages_str = message_store.read()
            existing_messages = yaml.safe_load(messages_str)
    except FileNotFoundError:
        existing_messages = list()
    updated_message = existing_messages + new_message
    with open(store, 'w') as message_store:
        yaml.safe_dump(updated_message, message_store)


def get_messages_from_store(store=None):
    """Retrieve the messages in our message store
    """
    if store is None:
        store = DEFAULT_STORE
    try:
        with open(store, 'r') as message_store:
            messages = message_store.read()
            return yaml.safe_load(messages)
    except FileNotFoundError:
        return []